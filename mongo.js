/*

	Activity: (database: session30)

	//Aggregate to count the total number of items supplied by Red Farms Inc. ($count stage)

	//Aggregate to count the total number of items with price greater than 50. ($count stage)

	//Aggregate to get the average price of all fruits that are onSale per supplier.($group)

	//Aggregate to get the highest price of fruits that are onSale per supplier. ($group)

	//Aggregate to get the lowest price of fruits that are onSale per supplier. ($group)


	//save your query/commands in the mongo.js

*/

// ---------> STUDENT'S SOLUTION <---------
// NUMBER 1
db.fruits.aggregate([
  		{
  			$match: {supplier: "Red Farms Inc."}
  		},
  		{
  			$count: "itemsSupplied"
  		}

  	])

// NUMBER 2
db.fruits.aggregate([
  		{
  			$match: {price: {$gt: 50}}
  		},
  		{
  			$count: "priceGreaterThan50"
  		}

  	])

// NUMBER 3
db.fruits.aggregate([
		{
			$match: {onSale: true}
		},
		{
			$group : {_id: "$supplier", avgPrice: {$avg: "$price"}} 
		}
	])

// NUMBER 4
db.fruits.aggregate([
		{
			$match: {onSale: true}
		},
		{
			$group : {_id: "$supplier", highestPrice: {$max: "$price"}} 
		}
	])

// NUMBER 5
db.fruits.aggregate([
		{
			$match: {onSale: true}
		},
		{
			$group : {_id: "$supplier", lowestPrice: {$min: "$price"}} 
		}
	])
